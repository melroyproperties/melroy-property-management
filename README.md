Melroy Investments and Property Management offers top quality real estate services. We specialize in the multifamily industry and pride ourselves on the success of our clients. Whether you are a seasoned investor or just getting started, we can help you achieve your investment goals.

Address: 4241 Jutland Dr, #201, San Diego, CA 92117, USA

Phone: 858-780-5111
